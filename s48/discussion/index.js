let posts = [];
let count = 1;

// add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) =>{
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully added.')
});

// shows posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`;

	});
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		// value of post [i] is a number while document.querySelector('#txt-edit-id').value is a string
		// therefore it is necessary to convert the number to a string first

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated');

			break
		}

	}
})

// delete post

const deletePost = (id) => {
    // find the index of the post with the given id
    const index = posts.findIndex(post => post.id.toString() === id);

    if (index !== -1) {
        // remove the post from the array using splice
        posts.splice(index, 1);

        // remove the post element from the DOM
        const postElement = document.querySelector(`#post-${id}`);
        postElement.remove();
    }
}