const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


function updateFullName() {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  spanFullName.textContent = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);