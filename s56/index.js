function countLetter(letter, sentence) {
    let result = 0;

 
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

 
    const characters = sentence.split('');

 
    for (let i = 0; i < characters.length; i++) {
        if (characters[i] === letter) {
            result++;
        }
    }

    // Return the count.
    return result;
}
console.log(countLetter('a', 'apple')); 

function isIsogram(text) {

    const lowerText = text.toLowerCase();

 
    const letterFrequency = {};

    for (let i = 0; i < lowerText.length; i++) {
        const letter = lowerText[i];
        if (letterFrequency[letter]) {
            return false;
        }
        letterFrequency[letter] = 1;
    }

    return true;
}


console.log(isIsogram('hello')); 
console.log(isIsogram('World')); 

function purchase(age, price) {


    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = Math.round(price * 0.8);
        return discountedPrice.toString();
    } else {
        const roundedPrice = Math.round(price);
        return roundedPrice.toString();
    }
}


console.log(purchase(10, 50)); 
console.log(purchase(18, 50)); 

function findHotCategories(items) {


    const hotCategories = [];
    const categories = [];

    for (const item of items) {
        if (!categories.includes(item.category)) {
            categories.push(item.category);
        }
    }

 
    for (const category of categories) {
        const itemsInCategory = items.filter(item => item.category === category);
        const hasNoStocks = itemsInCategory.every(item => item.stocks === 0);
        if (hasNoStocks) {
            hotCategories.push(category);
        }
    }

    return hotCategories;
}

const items = [    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }];


console.log(findHotCategories(items)); 

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
}

// Test the function
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];
const result = findFlyingVoters(candidateA, candidateB);

// Check if the result matches the expected output
if (JSON.stringify(result) === JSON.stringify(['LIWf1l', 'V2hjZH'])) {
    console.log('Test passed');
} else {
    console.log('Test failed');
}

/*module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};*/